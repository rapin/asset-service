const yargs = require('yargs');
const loginService = require('./login-service');
yargs.command(
    {
        command: "login",
        descript: "Please input username",
        builder: {
            username: {
                describe: "usernme for login",
                demandOption: true,
                type: "string"
            }
        },
        handler(args) {
            console.log(args.username)
            loginService.login(args.username);
        }
    }
);
yargs.parse();