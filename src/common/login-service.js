const fs = require('fs');

const readUserInfo = (username) => {
    try {
        const userInfo = fs.readFileSync("data/user-info.json");
        return JSON.parse(userInfo);
    } catch (error) {
        console.log("File not found");
    }

};

const login = (username) => {
    const allUser = readUserInfo();
    for (const iterator of allUser) {
        if(username === iterator.username){
            console.log("Login success.");
            break;
        }else{
            console.log("Login fail !.")
        }
    }
    
};
module.exports = {login}